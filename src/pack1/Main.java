package pack1;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		
		ListOfNumbers myList = new ListOfNumbers();
		FileReader fl=new FileReader();
		myList.numbers=fl.readList();
		System.out.print("The list: ");
		myList.printList();
		System.out.println("Largest number: " + myList.largestNumber());
		System.out.println("Smallest number: " + myList.smallestNumber());
		System.out.print("Palindromes: ");
		myList.printPalindromes();
		System.out.print("Prime numbers: ");
		myList.printPrimeNumbers();

	}

}