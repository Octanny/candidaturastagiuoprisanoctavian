package pack1;

import java.util.*;

public class ListOfNumbers {

	protected List<Integer> numbers;

	public int largestNumber() {
		int max;
		if (numbers.size() != 0) {
			max = numbers.get(0);
		} else {
			System.out.println("The list is empty");
			return 0;
		}
		for (final int i : numbers) {
			if (i > max) {
				max = i;
			}
		}
		return max;
	}

	public int smallestNumber() {
		int min;
		if (numbers.size() != 0) {
			min = numbers.get(0);
		} else {
			System.out.println("The list is empty");
			return 0;
		}
		for (final int i : numbers) {
			if (i < min) {
				min = i;
			}
		}
		return min;
	}

	public boolean palindrome(final int number) {//check if a number is palindrome
		int x, reverseX = 0;
		x = number;
		while (x != 0) {
			reverseX = reverseX * 10 + x % 10;
			x = x / 10;
		}
		return (number == reverseX);
	}

	public void printPalindromes() {
		boolean foundOne=false;
		for (final int i : numbers) {
			if (palindrome(i) == true) {
				foundOne=true;
				System.out.print(i + " ");
			}
		}
		if(foundOne==true)
			System.out.println();
		else System.out.println("The list does not contain any palindrome numbers");
	}

	public boolean prime(final int number) {//check if a number is prime
		if (number == 1 || number == 2)
			return true;
		int k;
		if (number % 2 == 0) {
			k = number;
		} else {
			k = 3;
			while (k < Math.sqrt(number)) {
				if (number % k == 0)
					k = number;
				else
					k += 2;
			}
		}
		return (k != number);
	}

	public void printPrimeNumbers() {
		boolean foundOne=false;
		for (final int i : numbers) {
			if (prime(i) == true) {
				foundOne=true;
				System.out.print(i + " ");
			}
		}
		if(foundOne==true)
			System.out.println();
		else System.out.println("The list does not contain any prime numbers");
	}

	public void printList() {
		for (final int i : numbers)
			System.out.print(i + " ");
		System.out.println();
	}
}
