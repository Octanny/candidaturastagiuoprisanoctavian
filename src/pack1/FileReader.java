package pack1;

import java.io.*;
import java.util.*;

public class FileReader extends ListOfNumbers {
	private Scanner scanner;

	public void openFile() {
		try {
			scanner = new Scanner(new File("InputData.txt"));
		} catch (Exception e) {
			System.out.println("Could not find the file \n");
		}
	}

	public List<Integer> readList() throws FileNotFoundException {
		this.openFile();
		List<Integer> numbers = new ArrayList<Integer>();
		while (scanner.hasNextInt()) {
			numbers.add(scanner.nextInt());
		}
		scanner.close();
		return numbers;
	}
}
