This is an application that reads from a file a list of integers and executes the following operations:
1)	Prints the list;
2)	Finds the largest number;
3)	Finds the smallest number;
4)	Prints any numbers that are palindromes;
5)	Prints any numbers that are prime numbers.

Observation: The numbers are read from the file “InputData.txt”, using the class FileReader.  This class contains a method called readList, which returns the list of integers.

Example: Given a list of [234, 678, 131, 101, 199, 346], the output is:
The list: 234 678 131 101 199 346
Largest number: 678
Smallest number: 101
Palindromes: 131, 101
Prime numbers: 131, 101, 199